#!/usr/bin/env node

import fs from 'fs'
import { makeBadge, ValidationError } from 'badge-maker'
import { XMLParser } from 'fast-xml-parser'


import { hero } from './hero.js'


function generateStars(level) {
  return Array.from(Array(5)).reduce((acc, _, i) => i >= level ? acc + '☆' : acc + '★', '')
}


async function fetchBadge(format) {
  try {
    const res = await fetch(`https://img.shields.io/static/v1?message=${format.message}&logo=${format.logo}&labelColor=5c5c5c&color=${format.color}&label=${format.label}&style=${format.style}`)
    const payload = await res.text()
    return payload
  } catch (e) {
    throw new Error(`Error retrieving badge: ${e}`, e)
  }
}


function createLevelBadge(level) {
  const format = {
    logo: hero,
    label: 'Level',
    style: 'for-the-badge',
    message: generateStars(level),
  }

  switch(level) {
    case 5: format.color = 'd9af0a'; break // gold
    case 4: format.color = '8b9597'; break // silver
    case 3: format.color = '8e6e41'; break // bronze
    case 2: format.color = 'green'; break // bronze
    default: format.color = 'grey'; break
  }

  return fetchBadge(format)
}


function createLinterBadge(errors) {
  const format = {
    label: 'Code quality'
  }

  if(errors === 0) {
    format.message = 'perfect'
    format.color = 'brightgreen'
  } else if(errors < 10) {
    format.message = 'good'
    format.color = 'green'
  } else if(errors < 50) {
    format.message = 'average'
    format.color = 'yellowgreen'
  } else {
    format.message = 'poor'
    format.color = 'red'
  }

  return fetchBadge(format)
}



function createTestsBadge(suite, percent) {
  const format = {
    label: suite,
    message: `${percent}%`
  }

  if(percent === 100) format.color = 'brightgreen'
  else if(percent > 80) format.color = 'green'
  else if(percent > 60) format.color = 'yellowgreen'
  else if(percent > 40) format.color = 'yellow'
  else if(percent > 20) format.color = 'orange'
  else format.color = 'inactive'

  return fetchBadge(format)
}


function writeBadge(filename, content) {
  try {
    fs.writeFileSync(filename, content);
  } catch (e) {
    throw new Error(`Error writing file: ${e}`, e)
  }
}


function readReport(filename) {
  const parser = new XMLParser({ ignoreAttributes: false, attributeNamePrefix: '__' })

  try {
    const data = fs.readFileSync(filename, 'utf8');
    return parser.parse(data)
  } catch (e) {
    throw new Error(`Error reading file: ${e}`, e)
  }
}


async function processReports() {
  // Linter
  const lintReport = readReport('junit-lint.xml')
  const lintErrors = lintReport.testsuites.testsuite.reduce((sum, ts) => sum + parseInt(ts.__errors), 0)
  const lintBadge = await createLinterBadge(lintErrors)
  writeBadge('badges/linter.svg', lintBadge)

  // Tests
  const testsReport = readReport('junit.xml')
  const testsInfo = testsReport.testsuites.testsuite.map(ts => {
    const errCount = parseInt(ts.__errors) + parseInt(ts.__failures)
    return {
      name: ts.__name,
      errors: errCount,
      percent: Math.round(100 - (errCount * 100 / parseInt(ts.__tests)))
    }
  })

  testsInfo.forEach(async ({ name, percent }) => {
    const badge = await createTestsBadge(name, percent)
    writeBadge(`badges/${name}.svg`, badge)
  })

  // Global level
  let hero = 0
  if(lintErrors === 0) hero++
  hero += testsInfo.reduce((sum, ts) => ts.errors === 0 ? sum + 1 : sum, 0)

  const heroBadge = await createLevelBadge(Math.round(hero * 5 / 5))
  writeBadge('badges/hero.svg', heroBadge)
}

processReports()
